# Importing Libraries
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import nltk
from nltk.corpus import stopwords
from nltk.stem.porter import PorterStemmer
import re

# Importing Datasets
data = pd.read_csv("GroceryStoreDataSet.csv", header = None)
# Checking For Null Values
data.isnull().sum()
# Description Of The Data
data.info()

# Clearing The Text

# To Count The Most Common Words in a Column- 
# 1) Clear (optional) then Split
# 2) Append Each Word In Separate Rows of a Temporary List
# 3) Value Count and Countplot

X = data.iloc[:, 0] # X = data[data.xyz == i].mn incase we have to do it WRT another column
for i in range(0, len(X)):
    X.iloc[i] = re.sub('[^a-zA-Z]', ' ', X.iloc[i])
    X.iloc[i] = X.iloc[i].lower()
    X.iloc[i] = X.iloc[i].split()
temp = []
for i in X:
    for j in i:
        temp.append(j)

# Most Common Words
most_common_products = pd.DataFrame(temp) # Converting List To DataFrame (DF)
y = most_common_products.iloc[:, 0] # DF To Series As We Can't ValueCounts or Visualize a DF
y.value_counts()

# Visualizing The Most Common Words
plt.figure(figsize = (10,5))
sns.countplot(x = y, order = y.value_counts().index[0:9])
plt.xlabel("Product Names")
plt.title("Product Which Was Sold The Most")

# Apriori
# Make Sure That The Series We Take Has Split Words
from apyori import apriori
apriori_list = X.tolist() # Converting Series Into List As It Is Better To Input Series as a Param
rules = apriori(apriori_list, min_support = 0.003, min_confidence = 0.2, min_lift = 3, min_length = 2)


# Visualizing theresults
results = list(rules)
    
    
